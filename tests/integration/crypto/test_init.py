import tempfile

import pytest
import yaml

RAW_YAML = """
provider:
  profile:
    kwarg1: value1
    list1:
      - value0
      - value1
"""
PARAMETRIZE = dict(argnames="crypto_plugin", argvalues=["fernet", "aesgcm256"])


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_generate_key(hub, crypto_plugin):
    # Verify that generating a key is successful
    key = await hub.crypto.init.generate_key(crypto_plugin)
    assert isinstance(key, str)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_encrypt(hub, crypto_plugin):
    data = yaml.safe_load(RAW_YAML)

    key = await hub.crypto.init.generate_key(crypto_plugin)
    encrypted = await hub.crypto.init.encrypt(data=data, key=key, plugin=crypto_plugin)
    assert encrypted != data
    assert encrypted


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_decrypt(hub, crypto_plugin):
    data = yaml.safe_load(RAW_YAML)

    key = await hub.crypto.init.generate_key(crypto_plugin)
    encrypted = await hub.crypto.init.encrypt(data=data, key=key, plugin=crypto_plugin)
    decrypted = await hub.crypto.init.decrypt(
        data=encrypted, key=key, plugin=crypto_plugin
    )
    assert decrypted == data


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_encrypt_file(hub, crypto_plugin):
    with tempfile.NamedTemporaryFile(delete=True) as cleartext_data:
        cleartext_data.write(RAW_YAML.encode())
        cleartext_data.flush()

        with tempfile.NamedTemporaryFile(delete=True) as fh:
            key = await hub.crypto.init.encrypt_file(
                crypto_plugin=crypto_plugin,
                acct_file=cleartext_data.name,
                output_file=fh.name,
            )
            assert key


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_decrypt_file(hub, crypto_plugin):
    with tempfile.NamedTemporaryFile(delete=True) as cleartext_data:
        cleartext_data.write(RAW_YAML.encode())
        cleartext_data.flush()

        with tempfile.NamedTemporaryFile(delete=True) as encoded_data:
            key = await hub.crypto.init.encrypt_file(
                crypto_plugin=crypto_plugin,
                acct_file=cleartext_data.name,
                output_file=encoded_data.name,
            )
            data = await hub.crypto.init.decrypt_file(
                crypto_plugin=crypto_plugin, acct_file=encoded_data.name, acct_key=key
            )

            assert data == yaml.safe_load(RAW_YAML)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_decrypt_plaintext_file(hub, crypto_plugin):
    with tempfile.NamedTemporaryFile(delete=True) as cleartext_data:
        cleartext_data.write(RAW_YAML.encode())
        cleartext_data.flush()

        data = await hub.crypto.init.decrypt_file(
            crypto_plugin=crypto_plugin,
            acct_file=cleartext_data.name,
            acct_key=None,
            render_pipe="yaml",
        )

        assert data == yaml.safe_load(RAW_YAML)
