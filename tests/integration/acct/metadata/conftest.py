import pathlib
import sys
import unittest.mock as mock

import pop.hub
import pytest


@pytest.fixture(scope="function", name="hub")
def tpath_hub():
    """
    Add "idem_plugin" to the test path
    """
    code_dir = pathlib.Path(__file__).parent.parent.parent.parent.parent.absolute()
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.create()

        hub.pop.sub.add(dyne_name="acct")

        with mock.patch("sys.argv", ["acct"]):
            hub.pop.config.load(["acct"], cli="acct")

        yield hub

        hub.pop.loop.CURRENT_LOOP.close()
