Extras
======

The `--extras` cli flag can be used to render a nested json string to `hub.OPT.acct.extras`.

.. code-block:: bash

    acct --extras="{'key1': 'value1', 'key2': 'value2'}"

The equivalent can be used in a config file

.. code-block:: yaml

    acct:
      extras:
        key1: value1
        key2: value2


Unlock functions and gather functions can now make use of this extra configuration

.. code-block:: python

    def gather(hub):
        print(hub.OPT.acct.extras)
        # {"key1": "value1", "key2": "value2"}


    async def unlock(hub):
        print(hub.OPT.acct.extras)
        # {"key1": "value1", "key2": "value2"}
