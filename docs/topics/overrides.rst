Extras
======

The `overrides` config option can be used to override specific attributes of profiles in the acct file.

Consider the following sls files.  The first defines a profile named "profile" under a provider named "provider".
.. code-block:: yaml

    # credentials.yaml

    provider:
      profile:
        key1: value1
        key2: value2

The acct overrides config option is used to override the value of "key1" with "new_value".

.. code-block:: yaml

    # config.yaml

    acct:
      overrides:
        provider:
          profile:
            key1: new_value
